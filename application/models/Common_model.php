<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Common_model extends CRM_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contract_types_model');
    }

    
    public function convertModuleTasks($from,$to,$fromId,$toId){
		
		$result = $this->db->select('*')->from('tblstafftasks')->where('rel_id',$fromId)->where('rel_type',$from)->get()->result_array();
		

		if($result){
			foreach($result AS $row){
				$data['name'] 					= $row['name'];
				$data['description'] 			= $row['description'];
				$data['priority'] 				= $row['priority'];
				$data['dateadded'] 				= $row['dateadded'];
				$data['startdate'] 				= $row['startdate'];
				$data['duedate'] 				= $row['duedate'];
				$data['datefinished'] 			= $row['datefinished'];
				$data['addedfrom'] 				= $row['addedfrom'];
				$data['is_added_from_contact'] 	= $row['is_added_from_contact'];
				$data['status'] 				= $row['status'];
				$data['recurring_type'] 		= $row['recurring_type'];
				$data['repeat_every'] 			= $row['repeat_every'];
				$data['recurring'] 				= $row['recurring'];
				$data['is_recurring_from'] 		= $row['is_recurring_from'];
				$data['cycles'] 				= $row['cycles'];
				$data['total_cycles'] 			= $row['total_cycles'];
				$data['custom_recurring'] 		= $row['custom_recurring'];
				$data['last_recurring_date'] 	= $row['last_recurring_date'];
				$data['rel_id'] 				= $toId;
				$data['rel_type'] 				= $to;
				$data['is_public'] 				= $row['is_public'];
				$data['billable'] 				= $row['billable'];
				$data['billed'] 				= $row['billed'];
				$data['invoice_id'] 			= $row['invoice_id'];
				$data['hourly_rate'] 			= $row['hourly_rate'];
				$data['milestone'] 				= $row['milestone'];
				$data['kanban_order'] 			= $row['kanban_order'];
				$data['milestone_order'] 		= $row['milestone_order'];
				$data['visible_to_client'] 		= $row['visible_to_client'];
				$data['deadline_notified'] 		= $row['deadline_notified'];
				
				$this->db->insert('tblstafftasks',$data);
				
			}
		}
		
		return TRUE;

		
	}
   
   
   


 
}
