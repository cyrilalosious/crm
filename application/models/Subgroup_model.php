<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Subgroup_model extends CRM_Model
{
    private $perm_statements = ['view', 'view_own', 'edit', 'create', 'delete'];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add new employee role
     * @param mixed $data
     */
    public function add($data)
    {
       

        $this->db->insert('tblsubgroup', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
           
            logActivity('New Sub Task Added [ID: ' . $insert_id . '.' . $data['name'] . ']');

            return $insert_id;
        }

        return false;
    }
	
	public function getGroup(){
		return $this->db->select('*')->from('tblgroups')->get()->result_array();
		
	}

   
    public function update($data, $id)
    {
        $affectedRows = 0;
        
        $this->db->where('id', $id);
        $this->db->update('tblsubgroup', $data);
        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
        }

        if ($affectedRows > 0) {
            logActivity('Sub Task  Updated [ID: ' . $id . '.' . $data['name'] . ']');

            return true;
        }

        return false;
    }

    /**
     * Get employee role by id
     * @param  mixed $id Optional role id
     * @return mixed     array if not id passed else object
     */
    public function get($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);

            return $this->db->get('tblsubgroup')->row();
        }

        return $this->db->get('tblsubgroup')->result_array();
    }

    /**
     * Delete employee role
     * @param  mixed $id role id
     * @return mixed
     */
    public function delete($id)
    {
        $current = $this->get($id);
        // Check first if role is used in table
        if (is_reference_in_table('role', 'tblstaff', $id)) {
            return [
                'referenced' => true,
            ];
        }
        $affectedRows = 0;
        $this->db->where('id', $id);
        $this->db->delete('tblsubgroup');
        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
        }
        
        if ($affectedRows > 0) {
            logActivity('Sub Task Deleted [ID: ' . $id);

            return true;
        }

        return false;
    }

   
}
