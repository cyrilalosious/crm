<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Freshdesk calls
 * 
 * Provides access to various Freshdesk APIs within the CodeIgniter PHP Framework.
 * @param mixed description 'HTML content of the ticket.'' 
 * @param mixed subject 'Subject of the ticket. The default Value is null'
 * @param mixed email 'Email address of the requester.'
 * @param boolean priority 'Priority of the ticket. The default value is 1.'
 * @param string email_config_id 'ID of email config which is used for this ticket'
 * @param boolean status 'Status of the ticket. The default Value is 2.''
 * @param mixed attachments 'Ticket attachments. The total size of these attachments cannot exceed 15MB.'
 */
function send_mail_freshdesk($description='', $subject='', $email='', $email_config_id = 1, $attachments = [], $status = 2,$priority = 1)
{
    $CI = & get_instance();
    if($email && $email_config_id && $subject){
        $ticket_data = array(
          "description" => $description,
          "subject" => $subject,
          "email" => $email,
          "priority" => $priority,
          "status" => $status,
          "email_config_id" => $email_config_id,
        );    
        if(isset($attachments)){
            $ticket_data["attachments"] = $attachments;
        }
        return $CI->freshdesk->api->create('tickets/outbound_email',$ticket_data);
    }
    return FALSE;
}