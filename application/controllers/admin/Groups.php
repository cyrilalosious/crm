<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Groups extends Admin_controller
{
    public function __construct()
    {
        parent::__construct();
        // Model is autoloaded
    }

    /* List all staff roles */
    public function index()
    {
        if (!has_permission('roles', '', 'view')) {
            access_denied('roles');
        }
        if ($this->input->is_ajax_request()) {
            $this->app->get_table_data('groups');
        }
        $data['title'] = _l('all_task_groups');
        $this->load->view('admin/groups/manage', $data);
    }

    /* Add new role or edit existing one */
    public function group($id = '')
    {
        if (!has_permission('roles', '', 'view')) {
            access_denied('roles');
        }
        if ($this->input->post()) {
            if ($id == '') {
                if (!has_permission('roles', '', 'create')) {
                    access_denied('roles');
                }
                $id = $this->groups_model->add($this->input->post());
                if ($id) {
                    set_alert('success', _l('added_successfully', _l('task_group')));
                    redirect(admin_url('groups/group/' . $id));
                }
            } else {
                if (!has_permission('roles', '', 'edit')) {
                    access_denied('roles');
                }
                $success = $this->groups_model->update($this->input->post(), $id);
                if ($success) {
                    set_alert('success', _l('updated_successfully', _l('task_group')));
                }
                redirect(admin_url('groups/group/' . $id));
            }
        }
        if ($id == '') {
            $title = _l('add_new', _l('task_group_lowercase'));
        } else {
           
            $group                     = $this->groups_model->get($id);
            $data['group']             = $group;
            $title                    = _l('edit', _l('task_group_lowercase')) . ' ' . $group->name;
        }
        
        $data['title']       = $title;
        $this->load->view('admin/groups/group', $data);
    }

    /* Delete staff role from database */
    public function delete($id)
    {
        if (!has_permission('roles', '', 'delete')) {
            access_denied('roles');
        }
        if (!$id) {
            redirect(admin_url('groups'));
        }
        $response = $this->groups_model->delete($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', _l('is_referenced', _l('task_group_lowercase')));
        } elseif ($response == true) {
            set_alert('success', _l('deleted', _l('task_group')));
        } else {
            set_alert('warning', _l('problem_deleting', _l('task_group_lowercase')));
        }
        redirect(admin_url('groups'));
    }
}
