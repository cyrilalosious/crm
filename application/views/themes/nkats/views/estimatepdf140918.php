<?php
// $pdf->AddPage();
$dimensions = $pdf->getPageDimensions();



// $pdf->MultiCell(0, 30, '', 0, 'J', 0, 1, '', '', true, 0, true, false, 0);

$info_right_column = '';
$info_left_column = '';
$pdf->ln(20);
// write the first column
if($pdf_custom_fields[0]){
   $value = get_custom_field_value($estimate->id,$pdf_custom_fields[0]['id'],'estimate');
    $info_left_column .= $pdf_custom_fields[0]['name'] . ': A'.$estimate->id.'<br /></br>'; 
}

if($pdf_custom_fields[0]){
   $value = get_custom_field_value($estimate->id,$pdf_custom_fields[1]['id'],'estimate');
    $info_right_column .= $pdf_custom_fields[1]['name'] . ': ' . $value. '<br /></br>'; 
    $info_right_column .= $pdf_custom_fields[39]['name'] . ': ' . $estimate->date . '<br /></br>'; 
}

$pdf->MultiCell(($dimensions['wk'] / 2) - $dimensions['lm'], 0, $info_left_column, 0, 'J', 0, 0, '', '', true, 0, true, true, 0);
// write the second column
$pdf->MultiCell(($dimensions['wk'] / 2) - $dimensions['rm'], 0, $info_right_column, 0, 'R', 0, 1, '', '', true, 0, true, false, 0);
// $pdf->ln(10);

$info_left_column2 .='</br><p>To,</p><p><b>M/s.</b></p></br></br><span class="sub"><b>Sub:</b> Supply & Installation of <u>'.explode(" ",get_custom_field_value($estimate->id,$pdf_custom_fields[3]['id'],'estimate'))[0].'</u> Passenger Elevator for your prestigious project at Chennai.</p><p>Asian Lifts, an innovative leader in the Elevator Industry. Asian Lifts has decade of experience in
manufacturing and installing world-class elevators. We go to great lengths in using our expertise to
ensure you to get the best value possible. We have a dedicated design and flexible manufacturing system
to deliver customized lifts. We can make a difference by serving our customers better than anybody else.</span></br><p>We adhere to the ISO 9001:2008 standards and work towards excellence in safety, quality & best service
which help us to achieve 100% customer satisfaction</p></br><p> The passion of our Asian Lifts is making people’s journey safe, convenient and reliable since 2003 </p></br><p> Our intention is to deliver the best quality and exceed customer service. Our quality culture encompasses
our products, processes and all the services we offer from design to maintenance and modernization. </p></br><p> We have a unique footprint with operations over 15 years which means we have a broader reach for our
brands than any other. We have nationally diverse, dynamic, committed and entrepreneurial team of
employees. </p></br><p>We act with Integrity for the total life cycle of our products and services and ensure the safety of users and
our people. We keep our promises. We drive new ideas to realization with speed and an obsession for
customer-driven quality. We thrive on challenges and take pride in our “can do” attitude. And never
compromise on our business ethics. This is the cornerstone of who we are as a company and is essential to
our future success. </p></br><p> We honour ethics and abide by the laws & regulations and we complete our job with no harm to people. </p></br><p> We are genuinely client focused and continually seeking improvements in our products. We are determined
to succeed and draw inspiration from challenges. </p></br>
</br>
<p><b>Thanking You</b></p></br><p><b> Yours Sincerely,</b></p>';
$info_left_column2 .='<p><b>For Asian Lifts</br>';


$pdf->MultiCell(0, 0, $info_left_column2, 0, 'J', 0, 1, '', '', true, 0, true, false, 0);


// Get Y position for the separation
$y            = $pdf->getY();
$organization_info = '<div style="color:#424242;">';
    $organization_info .= format_organization_info();
$organization_info .= '</div>';

$pdf->writeHTMLCell(($swap == '1' ? ($dimensions['wk']) - ($dimensions['lm'] * 2) : ($dimensions['wk'] / 2) - $dimensions['lm']), '', '', $y, $organization_info, 0, 0, false, true, ($swap == '1' ? 'R' : 'J'), true);

// // Estimate to
// $estimate_info = '<b>' ._l('estimate_to') . '</b>';
// $estimate_info .= '<div style="color:#424242;">';
// $estimate_info .= format_customer_info($estimate, 'estimate', 'billing');
// $estimate_info .= '</div>';

// // ship to to
// if($estimate->include_shipping == 1 && $estimate->show_shipping_on_estimate == 1){
//     $estimate_info .= '<br /><b>' . _l('ship_to') . '</b>';
//     $estimate_info .= '<div style="color:#424242;">';
//     $estimate_info .= format_customer_info($estimate, 'estimate', 'shipping');
//     $estimate_info .= '</div>';
// }

// $estimate_info .= '<br />'._l('estimate_data_date') . ': ' . _d($estimate->date).'<br />';

// if (!empty($estimate->expirydate)) {
//     $estimate_info .= _l('estimate_data_expiry_date') . ': ' . _d($estimate->expirydate) . '<br />';
// }

// if (!empty($estimate->reference_no)) {
//     $estimate_info .= _l('reference_no') . ': ' . $estimate->reference_no. '<br />';
// }

// if($estimate->sale_agent != 0 && get_option('show_sale_agent_on_estimates') == 1){
//     $estimate_info .= _l('sale_agent_string') . ': ' .  get_staff_full_name($estimate->sale_agent). '<br />';
// }

// if ($estimate->project_id != 0 && get_option('show_project_on_estimate') == 1) {
//     $estimate_info .= _l('project') . ': ' . get_project_name_by_id($estimate->project_id). '<br />';
// }

// foreach($pdf_custom_fields as $field){
//     $value = get_custom_field_value($estimate->id,$field['id'],'estimate');
//     if($value == ''){continue;}
//     $estimate_info .= $field['name'] . ': ' . $value. '<br />';
// }

// $pdf->writeHTMLCell(($dimensions['wk'] / 2) - $dimensions['rm'], '', '', ($swap == '1' ? $y : ''), $estimate_info, 0, 1, false, true, ($swap == '1' ? 'J' : 'R'), true);
$pdf->AddPage();
// The Table
$pdf->Ln(do_action('pdf_info_and_table_separator', 6));
$item_width = 38;
// If show item taxes is disabled in PDF we should increase the item width table heading
$item_width = get_option('show_tax_per_item') == 0 ? $item_width+15 : $item_width;
$custom_fields_items = get_items_custom_fields_for_table_html($estimate->id,'estimate');

// Calculate headings width, in case there are custom fields for items
$total_headings = get_option('show_tax_per_item') == 1 ? 4 : 3;
$total_headings += count($custom_fields_items);
$headings_width = (100-($item_width+2)) / $total_headings;

$qty_heading = _l('estimate_table_quantity_heading');
if($estimate->show_quantity_as == 2){
    $qty_heading = _l('estimate_table_hours_heading');
} else if($estimate->show_quantity_as == 3){
    $qty_heading = _l('estimate_table_quantity_heading') .'/'._l('estimate_table_hours_heading');
}


// Header
$tblhtml = '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="8">';

$tblhtml .= '<tr height="30" bgcolor="' . get_option('pdf_table_heading_color') . '" style="color:' . get_option('pdf_table_heading_text_color') . ';">';
$tblhtml .= '<th width="40%" align="left">' . _l('Technical Specification') . '</th>';
$tblhtml .= '<th width="60%;" align="center"></th>';
$tblhtml .= '</tr>';
$tblhtml .= '<tbody>';

// for($i=2;$i<12;$i++){
//     $tblhtml .= '
//             <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
//                 <td >'.$pdf_custom_fields[$i]['name'].'</td>
//                 <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[$i]['id'],'estimate') . '</td>
//             </tr>';
// }
// print_r (explode(" ",get_custom_field_value($estimate->id,$pdf_custom_fields[3]['id'],'estimate'))[0]);
//split("[ ]",get_custom_field_value($estimate->id,$pdf_custom_fields[3]['id'],'estimate'));

    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[2]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[2]['id'],'estimate') . '</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[3]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[3]['id'],'estimate') . '</td>
            </tr>';   
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[4]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[4]['id'],'estimate') . '</td>
            </tr>';
      $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[5]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[5]['id'],'estimate') . '</td>
            </tr>'; 
      $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[6]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[6]['id'],'estimate') . '</td>
            </tr>';
      $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[7]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[7]['id'],'estimate') . '</td>
            </tr>'; 
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[8]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[8]['id'],'estimate') . '</td>
            </tr>';           
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[9]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[9]['id'],'estimate') . '</td>
            </tr>';  
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[10]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[10]['id'],'estimate') . '</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[11]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[11]['id'],'estimate') . '</td>
            </tr>';                            
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'._l('Hoist Way Size').'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[12]['id'],'estimate') . ' mm'.get_custom_field_value($estimate->id,$pdf_custom_fields[13]['id'],'estimate').' mm</td>
            </tr>'; 
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[14]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[14]['id'],'estimate') . '</td>
            </tr>';     
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[15]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[15]['id'],'estimate') . '</td>
            </tr>';             


$tblhtml .= '</tbody>';
$tblhtml .= '</table>';

$pdf->writeHTML($tblhtml, true, false, false, false, '');


// Header
$tblhtml = '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="8">';

$tblhtml .= '<tr height="30" bgcolor="' . get_option('pdf_table_heading_color') . '" style="color:' . get_option('pdf_table_heading_text_color') . ';">';
$tblhtml .= '<th width="40%" align="left">' . _l('Car Interior , Safety & Features') . '</th>';
$tblhtml .= '<th width="60%;" align="center"></th>';
$tblhtml .= '</tr>';
$tblhtml .= '<tbody>';


    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'._l('Car Size (W x D x H)').'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[16]['id'],'estimate') . ' mm '.get_custom_field_value($estimate->id,$pdf_custom_fields[17]['id'],'estimate').' mm x 2100 mm</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[18]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[18]['id'],'estimate') . '</td>
            </tr>';   
     $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[19]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[19]['id'],'estimate') . '</td>
            </tr>';             
     $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[20]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[20]['id'],'estimate') . '</td>
            </tr>';   
      $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[21]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[21]['id'],'estimate') . '</td>
            </tr>';   
     $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[22]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[22]['id'],'estimate') . '</td>
            </tr>'; 
     $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[23]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[23]['id'],'estimate') . '</td>
            </tr>';  
     $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[24]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[24]['id'],'estimate') . '</td>
            </tr>'; 
     $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[25]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[25]['id'],'estimate') . '</td>
            </tr>';   
     $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[26]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[26]['id'],'estimate') . '</td>
            </tr>';   
      $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[27]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[27]['id'],'estimate') . '</td>
            </tr>';  
     $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[28]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[28]['id'],'estimate') . '</td>
            </tr>';  
     $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[29]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[29]['id'],'estimate') . '</td>
            </tr>';                                                                    
$tblhtml .= '</tbody>';
$tblhtml .= '</table>';

$pdf->writeHTML($tblhtml, true, false, false, false, '');

$pdf->AddPage();
// Header Car Door Specifications
$tblhtml = '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="8">';

$tblhtml .= '<tr height="30" bgcolor="' . get_option('pdf_table_heading_color') . '" style="color:' . get_option('pdf_table_heading_text_color') . ';">';
$tblhtml .= '<th width="40%" align="left">' . _l('Car Door Specifications') . '</th>';
$tblhtml .= '<th width="60%;" align="center"></th>';
$tblhtml .= '</tr>';
$tblhtml .= '<tbody>';


    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[30]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[30]['id'],'estimate') . '</td>
            </tr>';//mm x 2000 mm
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[31]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[31]['id'],'estimate') . '</td>
            </tr>';   
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[32]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[32]['id'],'estimate') . '</td>
            </tr>';              

$tblhtml .= '</tbody>';
$tblhtml .= '</table>';

$pdf->writeHTML($tblhtml, true, false, false, false, '');

// Header
$tblhtml = '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="8">';

$tblhtml .= '<tr height="30" bgcolor="' . get_option('pdf_table_heading_color') . '" style="color:' . get_option('pdf_table_heading_text_color') . ';">';
$tblhtml .= '<th width="40%" align="left">' . _l('Landing Specifications') . '</th>';
$tblhtml .= '<th width="60%;" align="center"></th>';
$tblhtml .= '</tr>';
$tblhtml .= '<tbody>';

    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[30]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[30]['id'],'estimate') . '</td>
            </tr>';//mm x 2000 mm
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[33]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[33]['id'],'estimate') . '</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[6]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[6]['id'],'estimate') . '</td>
            </tr>';        
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[40]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[40]['id'],'estimate') . '</td>
            </tr>'; 
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[28]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[28]['id'],'estimate') . '</td>
            </tr>';        
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[25]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[25]['id'],'estimate') . '</td>
            </tr>';               

$tblhtml .= '</tbody>';
$tblhtml .= '</table>';

$pdf->writeHTML($tblhtml, true, false, false, false, '');


// Header
$tblhtml = '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="8">';

$tblhtml .= '<tr height="30" bgcolor="' . get_option('pdf_table_heading_color') . '" style="color:' . get_option('pdf_table_heading_text_color') . ';">';
$tblhtml .= '<th width="40%" align="left">' . _l('Traction Specification') . '</th>';
$tblhtml .= '<th width="60%;" align="center"></th>';
$tblhtml .= '</tr>';
$tblhtml .= '<tbody>';


    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[34]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[34]['id'],'estimate') . '</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[35]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[35]['id'],'estimate') . '</td>
            </tr>';  
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'.$pdf_custom_fields[36]['name'].'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[36]['id'],'estimate') . '</td>
            </tr>'; 
     $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td >'._l('Rope').'</td>
                <td >' . get_custom_field_value($estimate->id,$pdf_custom_fields[37]['id'],'estimate') . ' mm '.get_custom_field_value($estimate->id,$pdf_custom_fields[38]['id'],'estimate').' mm x 2100 mm</td>
            </tr>';                      

$tblhtml .= '</tbody>';
$tblhtml .= '</table>';

$pdf->writeHTML($tblhtml, true, false, false, false, '');

// Header
$tblhtml = '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="8">';

$tblhtml .= '<tr height="30" bgcolor="' . get_option('pdf_table_heading_color') . '" style="color:' . get_option('pdf_table_heading_text_color') . ';">';
$tblhtml .= '<th width="40%" align="left">' . _l('Safety Features') . '</th>';
$tblhtml .= '<th width="60%;" align="center"></th>';
$tblhtml .= '</tr>';
$tblhtml .= '<tbody>';


    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Automatic Rescue Device</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Safety Rope for Over Speed Governor</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Mechanical Safety Limit Switches for Final Limit</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Buffer Spring Suspension for Car & Counterweight Frame</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Emergency Stop Switches for Pit & Car Top</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Entrance Safety Guard will be provide during Installation</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Safety Ladder for Pit Access</td>
            </tr>';                                               

$tblhtml .= '</tbody>';
$tblhtml .= '</table>';

$pdf->writeHTML($tblhtml, true, false, false, false, '');
$pdf->AddPage();
// Header
$tblhtml = '<table width="100%" bgcolor="#fff" cellspacing="0" cellpadding="8">';

$tblhtml .= '<tr height="30" bgcolor="' . get_option('pdf_table_heading_color') . '" style="color:' . get_option('pdf_table_heading_text_color') . ';">';
$tblhtml .= '<th width="40%" align="left">' . _l('Gian Technology Features') . '</th>';
$tblhtml .= '<th width="60%;" align="center"></th>';
$tblhtml .= '</tr>';
$tblhtml .= '<tbody>';


    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Inbuilt Stabilizer with Automatic Low Volt Adjusting Mode Up to 330V</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Inverter based auto closed loop ARD with Battery Preventer</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Automatic Sleep Mode Timer With Power Saving Up To 40%</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >3 Level Safety circuit Provided to prevent the over travel</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >High Level Accuracy program for Smooth & jerk Free Stopping</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Fault Information programmed Digital Display for car and all landing</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Automatic Cut off Program for High Volt & Low Volt protection</td>
            </tr>';
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Controller & Traction Protecting Preventer</td>
            </tr>';  
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Automatic Phase Reverse& forwarder</td>
            </tr>';  
    $tblhtml .= '
            <tr style="background-color:#f0f0f0;border-bottom-style: dotted;">
                <td ></td>
                <td >Sensor type floor counting reed switch</td>
            </tr>';                                                                         

$tblhtml .= '</tbody>';
$tblhtml .= '</table>';

$pdf->writeHTML($tblhtml, true, false, false, false, '');

$content='<span><strong><u>Delivery &Completion</u></strong></span>
<ul style="margin-left:30px;">
<li><b> Material Delivery: </b> 2 Stages within 4 Weeks from the date of advance with order
</li>
<li><b>Completion : </b>4 Weeks from the date of Complete Materials Arrival. </li>
</ul>';


$pdf->MultiCell(0, 0, $content, 0, 'J', 0, 1, '', '', true, 0, true, false, 0);
$pdf->ln(10);
$content='<span><strong><u>Payment Terms</u></strong></span>
<ul style="margin-left:30px;">
<li>30% in advance with order</li>
<li>30% on 1st Stage materials delivery</li>
<li>30% on 2nd Stage materials delivery</li>
<li>10% on handing over</li>
</ul>';


$pdf->MultiCell(0, 0, $content, 0, 'J', 0, 1, '', '', true, 0, true, false, 0);
$pdf->ln(10);
$content='<span><strong><u>Inclusive</u></strong></span>
<ul style="margin-left:30px;">
<li>5 Years Warranty</li>
<li>1 Year Free Service & Maintenance</li>
<li>Transportation</li>
</ul>';


$pdf->MultiCell(0, 0, $content, 0, 'J', 0, 1, '', '', true, 0, true, false, 0);
$pdf->ln(10);

$content='<span><strong><u>Customer Scope</u></strong></span>
<ul style="margin-left:30px;">
<li>Scaffolding</li>
<li>Civil Works</li>
<li>Electrical Works</li>
<li>Separate Store room need to provide for keep elevator components</li>
</ul>';


$pdf->MultiCell(0, 0, $content, 0, 'J', 0, 1, '', '', true, 0, true, false, 0);
$pdf->ln(10);
$pdf->AddPage();
$content='<span><strong><u>Offer Validity</u></strong></span>
<ul style="margin-left:30px;">
<li>30 Days from the offer date.</li>
</ul>';


$pdf->MultiCell(0, 0, $content, 0, 'J', 0, 1, '', '', true, 0, true, false, 0);
$pdf->ln(10);
$content='<span><strong><u>Offer Value</u></strong></span>
<ul style="margin-left:30px;">
<li>The cost for Installation & Commissioning 1 No. of '.explode(" ",get_custom_field_value($estimate->id,$pdf_custom_fields[3]['id'],'estimate'))[0].' Passenger Capacity Elevator with '.get_custom_field_value($estimate->id,$pdf_custom_fields[6]['id'],'estimate').'
Operated ........... Steel ............ Opening Door is INR </li>

</ul></br>';


$pdf->MultiCell(0, 0, $content, 0, 'J', 0, 1, '', '', true, 0, true, false, 0);
$pdf->ln(10);

// write the first column
    $info_left_column3 .= 'Best Regards<br />'; 
    $info_left_column3 .= 'For ASIAN LIFTS<br />';

    $info_right_column3 .= '</br>Accepted By<br />';
     $info_right_column3 .= '(Name / Signature / Designation / Date)<br />'; 


$pdf->MultiCell(($dimensions['wk'] / 2) - $dimensions['lm'], 0, $info_left_column3, 0, 'J', 0, 0, '', '', true, 0, true, true, 0);
// write the second column
$pdf->MultiCell(($dimensions['wk'] / 2) - $dimensions['rm'], 0, $info_right_column3, 0, 'R', 0, 1, '', '', true, 0, true, false, 0);
// $pdf->ln(10);
// $pdf->Ln(8);
// $tbltotal = '';
// $tbltotal .= '<table cellpadding="6" style="font-size:'.($font_size+4).'px">';
// $tbltotal .= '
// <tr>
//     <td align="right" width="85%"><strong>'._l('estimate_subtotal').'</strong></td>
//     <td align="right" width="15%">' . format_money($estimate->subtotal,$estimate->symbol) . '</td>
// </tr>';

// if(is_sale_discount_applied($estimate)){
//     $tbltotal .= '
//     <tr>
//         <td align="right" width="85%"><strong>' . _l('estimate_discount');
//         if(is_sale_discount($estimate,'percent')){
//             $tbltotal .= '(' . _format_number($estimate->discount_percent, true) . '%)';
//         }
//         $tbltotal .= '</strong>';
//         $tbltotal .= '</td>';
//         $tbltotal .= '<td align="right" width="15%">-' . format_money($estimate->discount_total, $estimate->symbol) . '</td>
//     </tr>';
// }

// foreach ($taxes as $tax) {
//     $tbltotal .= '<tr>
//     <td align="right" width="85%"><strong>' . $tax['taxname'] . ' (' . _format_number($tax['taxrate']) . '%)' . '</strong></td>
//     <td align="right" width="15%">' . format_money($tax['total_tax'], $estimate->symbol) . '</td>
// </tr>';
// }

// if ((int)$estimate->adjustment != 0) {
//     $tbltotal .= '<tr>
//     <td align="right" width="85%"><strong>'._l('estimate_adjustment').'</strong></td>
//     <td align="right" width="15%">' . format_money($estimate->adjustment,$estimate->symbol) . '</td>
// </tr>';
// }

// $tbltotal .= '
// <tr style="background-color:#f0f0f0;">
//     <td align="right" width="85%"><strong>'._l('estimate_total').'</strong></td>
//     <td align="right" width="15%">' . format_money($estimate->total, $estimate->symbol) . '</td>
// </tr>';

// $tbltotal .= '</table>';

// $pdf->writeHTML($tbltotal, true, false, false, false, '');

if(get_option('total_to_words_enabled') == 1){
     // Set the font bold
     $pdf->SetFont($font_name,'B',$font_size);
     $pdf->Cell(0, 0, _l('num_word').': '.$CI->numberword->convert($estimate->total,$estimate->currency_name), 0, 1, 'C', 0, '', 0);
     // Set the font again to normal like the rest of the pdf
     $pdf->SetFont($font_name,'',$font_size);
     $pdf->Ln(4);
}

if (!empty($estimate->clientnote)) {
    $pdf->Ln(4);
    $pdf->SetFont($font_name,'B',$font_size);
    $pdf->Cell(0, 0, _l('estimate_note'), 0, 1, 'L', 0, '', 0);
    $pdf->SetFont($font_name,'',$font_size);
    $pdf->Ln(2);
    $pdf->writeHTMLCell('', '', '', '', $estimate->clientnote, 0, 1, false, true, 'L', true);
}

if (!empty($estimate->terms)) {
    $pdf->Ln(4);
    $pdf->SetFont($font_name,'B',$font_size);
    $pdf->Cell(0, 0, _l('terms_and_conditions'), 0, 1, 'L', 0, '', 0);
    $pdf->SetFont($font_name,'',$font_size);
    $pdf->Ln(2);
    $pdf->writeHTMLCell('', '', '', '', $estimate->terms, 0, 1, false, true, 'L', true);
}
