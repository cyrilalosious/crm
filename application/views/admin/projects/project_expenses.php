    <style>
.modal-footer1 {
    padding: 18px;
    text-align: right;
    border-top: 1px solid #e5e5e5;
    margin-bottom: 12px;
    margin-top: 400px;
}
 .form-group1
 {
	 margin-bottom: -1px;
 }
.form-text {
   
    width: 250px;
}

.dropzoneDragArea1 {
    background-color: #fbfdff;
    border: 1px dashed #c0ccda;
    border-radius: 4px;
       padding: 30px;
    text-align: center;
    margin-bottom: 70px;
    cursor: pointer;
    width: 500px;
margin-left: 50px;}
.modal-sm1 {
		position:absolute;
  top:50% !important;
  transform: translate(0, -50%) !important;
  -ms-transform: translate(0, -50%) !important;
  -webkit-transform: translate(0, -50%) !important;
  margin:auto 25%;
  width:55%;
  
		}
</style>  


	 <div id="expenses_total"></div><hr />
      <?php if(has_permission('expenses','','create')){ ?>
      <a href="#" data-toggle="modal" data-target="#new_project_expense" class="btn btn-info mbot25"><?php echo _l('new_expense'); ?></a>
      <?php } ?>
      <?php
      $data_expenses_filter['total_unbilled'] = $this->db->query('SELECT count(*) as num_rows FROM tblexpenses WHERE (SELECT 1 from tblinvoices WHERE tblinvoices.id = tblexpenses.invoiceid AND status != 2)')->row()->num_rows;
      $data_expenses_filter['categories'] = $expense_categories;
      $data_expenses_filter['filter_table_name'] = '.table-project-expenses';
      $data_expenses_filter['years'] = $this->expenses_model->get_expenses_years();
      $this->load->view('admin/expenses/filter_by_template',$data_expenses_filter); ?>
      <div class="clearfix"></div>
      <?php
        echo form_hidden('custom_view');
        $this->load->view('admin/expenses/table_html', array('class'=>'project-expenses'));
      ?>
      <div class="modal fade" id="new_project_expense" tabindex="-1" role="dialog">
        <div class="modal-sm1">
          <div class="modal-content">
            <?php echo form_open(admin_url('projects/add_expense'),array('id'=>'project-expense-form','class'=>'dropzone dropzone-manual')); ?>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><?php echo _l('add_new', _l('expense_lowercase')); ?></h4>
            </div>
			
            <div class="modal-body">
             
            <div class="panel_s">
               <div class="panel-body">
			   
			    <div class="row">
				<div class="col-md-4">
				<div class="form-text">
			<?php echo render_input('expense_name','expense_name'); ?> </div></div>
			
                  <div class="col-md-4">
				  <div class="form-text">
             <?php echo render_select('category',$expense_categories,array('id','name'),'expense_category'); ?> </div></div>
			 
                  <div class="col-md-4">
				  <div class="form-text">
             <?php echo render_date_input('date','expense_add_edit_date',_d(date('Y-m-d'))); ?> </div></div><br>
				</div>
				<br>
				<div class="row">
				<div class="col-md-4">
			  <div class="form-text">
             <?php echo render_input('amount','expense_add_edit_amount','','number'); ?> </div></div>
			 
			 
			 
            <div class="hide">
              <div class="col-md-4">
			  <div class="form-text">
                <div class="form-group">
                  <label class="control-label" for="tax"><?php echo _l('tax_1'); ?></label>
                  <select class="selectpicker display-block" data-width="100%" name="tax" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>">
                    <option value=""><?php echo _l('no_tax'); ?></option>
                    <?php foreach($taxes as $tax){ ?>
                    <option value="<?php echo $tax['id']; ?>" data-subtext="<?php echo $tax['name']; ?>"><?php echo $tax['taxrate']; ?>%</option>
                    <?php } ?>
                  </select>
                </div> </div>
              </div>
              
			  
			  
              <div class="col-md-4">
			  
                 <label class="control-label" for="tax2"><?php echo _l('tax_2'); ?></label>
                 <select class="selectpicker display-block" data-width="100%" name="tax2" data-none-selected-text="<?php echo _l('dropdown_non_selected_tex'); ?>" disabled>
                  <option value=""><?php echo _l('no_tax'); ?></option>
                  
				  
				  <?php foreach($taxes as $tax){ ?>
                  <option value="<?php echo $tax['id']; ?>" data-subtext="<?php echo $tax['name']; ?>"><?php echo $tax['taxrate']; ?>%</option>
                  <?php } ?>
				  
                </select>
              </div>
            </div>
			
			 <div class="col-md-4">
		 <div class="form-text">
         <?php echo render_input('reference_no','expense_add_edit_reference_no'); ?> </div></div>
         <?php $selected = (isset($expense) ? $expense->paymentmode : ''); ?>

         <?php
               // Fix becuase payment modes are used for invoice filtering and there needs to be shown all
               // in case there is payment made with payment mode that was active and now is inactive
         $expenses_modes = array();
         foreach($payment_modes as $m){
          if(isset($m['invoices_only']) && $m['invoices_only'] == 1) {continue;}
          if($m['active'] == 1){
            $expenses_modes[] = $m;
          }
        }
        ?>
		
		
		<div class="col-md-4">
		<div class="form-text">
        <?php echo render_select('paymentmode',$expenses_modes,array('id','name'),'payment_mode',$selected); ?> </div></div>
			
			
		 
          <div class="hide">
           <?php echo render_select('currency',$currencies,array('id','name','symbol'),'expense_currency',$currency->id); ?>
         
        
		<div class="col-md-4">
		 
		<?php
         $customer_expense_data = array();
         $_customer_expense_data = array();
         $_customer_expense_data['userid'] = $project->client_data->userid;
         $_customer_expense_data['company'] = $project->client_data->company;
         $customer_expense_data[] = $_customer_expense_data;
         echo render_select('clientid',$customer_expense_data,array('userid','company'),'expense_add_edit_customer',$project->clientid); ?>
        </div> </div> 
				</div>
				
				<div class="row">
				<div class="col-md-4">
		
		<div class="checkbox checkbox-primary">
           <input type="checkbox" id="billable" name="billable" checked>
           <label for="billable"><?php echo _l('expense_add_edit_billable'); ?></label>
         </div> 
		 </div> 
		 
		  
		
				</div>
				<div class="row">
				<div class="form-text">
		<div class="col-md-4">
		 <div id="dropzoneDragArea" class="dz-default dz-message glyphicon glyphicon-file" style="padding: 20px;">
			 
               <span><?php echo _l('expense_add_edit_attach_receipt'); ?></span>
             </div>  </div> </div> 
			 
			 
			 <div class="col-md-4">
             <div class="dropzone-previews"></div>
             <i class="fa fa-question-circle" data-toggle="tooltip" data-title="<?php echo _l('expense_name_help'); ?>"></i></div>
		
        <div class="clearfix mbot15"></div>
        <?php echo render_custom_fields('expenses'); ?>
        <?php echo form_hidden('project_id',$project->id); ?>
        <?php echo form_hidden('clientid',$project->clientid); ?>
        <div class="clearfix"></div>
				
				
				</div>
				
				
				<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
        <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
      </div> 
	  
	   
      <?php echo form_close(); ?>
	  
				
				
			 </div>	<!-- /.panel_s -->
          </div>   <!-- /.panel-body -->      
	   </div><!-- /.modal-body -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
 
	  