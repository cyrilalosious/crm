<p><?php echo _l('project_note_private'); ?></p>
<hr />
<?php echo form_open(admin_url('projects/save_note/'.$project->id)); ?>
<?php echo render_textarea('content','',$staff_notes,array(),array(),'','tinymce'); ?>
<button type="submit" class="btn btn-info"><?php echo _l('project_save_note'); ?></button>
<?php echo form_close(); ?>
<hr />
<p><a onclick="get_sales_notes(<?php echo $project->id; ?>,'projects'); return false">Public note</a></p>
<div role="tabpanel" class="tab-pane" id="tab_notes">
   <?php echo form_open(admin_url('projects/add_note/'.$project->id),array('id'=>'sales-notes','class'=>'project-notes-form')); ?>
   <?php echo render_textarea('description'); ?>
   <div class="text-right">
   	  <span onclick="get_sales_notes(<?php echo $project->id; ?>,'projects'); return false" class="btn btn-info mtop15 mbot15"><?php echo _l('Refresh'); ?></span>
      <button type="submit" class="btn btn-info mtop15 mbot15"><?php echo _l('estimate_add_note'); ?></button>
   </div>
   <?php echo form_close(); ?>
   <hr />
   <div class="panel_s mtop20 no-shadow" id="sales_notes_area">
   </div>
</div>