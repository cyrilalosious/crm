<?php init_head(); ?>
<div id="wrapper">
    <div class="content">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel_s">
                    <div class="panel-body">
                        <h4 class="no-margin">
                         <?php echo $title; ?>
                     </h4>
                         <hr class="hr-panel-heading" />
                     <?php if(isset($subgroup)){ ?>
                     <a href="<?php echo admin_url('groups/group'); ?>" class="btn btn-success pull-right mbot20 display-block"><?php echo _l('new_task_groups'); ?></a>
                     <div class="clearfix"></div>
                     <?php } ?>
                     <?php echo form_open($this->uri->uri_string()); ?>
						 
                    <?php $attrs = (isset($subgroup) ? array() : array('autofocus'=>true)); ?>
                    <?php $value = (isset($subgroup) ? $subgroup->name : ''); ?>
					<?php $disabled = (isset($subgroup) ?'disabled="disabled"' : ''); ?>
					
					<div class="form-group" app-field-wrapper="name">
						<label for="name" class="control-label"> <small class="req text-danger">* </small>Group</label>
						<select class="form-control" name="gid" <?= $disabled; ?>>
						<?php 
						foreach($grouplist AS $row){
							$selected = ($row['id']==$subgroup->gid?'selected="selected"':'');
							
							echo '<option  '.$selected.' value="'.$row['id'].'">'.$row['name'].'</option>';
							 
						}
						?>
						</select>
					</div>
					
					
					
                    <?php echo render_input('name','task_group_add_edit_name',$value,'text',$attrs); ?>
					<div class="table-responsive">

					<button type="submit" class="btn btn-info pull-right"><?php echo _l('submit'); ?></button>
					<?php echo form_close(); ?>
					</div>
                </div>
            </div>
         </div>
    </div>
</div>
            <?php init_tail(); ?>
            <script>
                $(function(){
                  _validate_form($('form'),{name:'required'});
                });
            </script>
        </body>
        </html>
