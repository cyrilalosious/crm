
<style>
  
		.modal-sm1 {
		position:absolute;
  top:50% !important;
  transform: translate(0, -50%) !important;
  -ms-transform: translate(0, -50%) !important;
  -webkit-transform: translate(0, -50%) !important;
  margin:auto 35%;
  width:30%;
  
		}
  </style>

<?php echo form_open_multipart(admin_url('tasks/send_task_email/'.$task_id),array('id'=>'task-email-form')); ?>
<div class="modal fade email-template" data-editor-id=".<?php echo 'tinymce-'.$task_id; ?>" id="_mail_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-sm1" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Send Mail to Client</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    	<div class="form-group">
							<?php
							$this->db->select('rel_id');
							$this->db->where('id',$task_id);
							$task = $this->db->get('tblstafftasks')->row();
							$rel_id=$task->rel_id;
							$this->db->select('clientid');
							$this->db->where('id',$rel_id);
							$projects=$this->db->get('tblprojects')->row();
							$client=$projects->clientid;
							
							$this->db->select('value');
							$this->db->where('relid',$client);
							$this->db->where('fieldto','customers');
							$this->db->where('fieldid','129');
							$customfield=$this->db->get('tblcustomfieldsvalues')->row();
							//print_r($customfield);
							/*if($customfield)
								$client_email=$customfield->value;
							else
								$client_email="";*/
							  $query1 = "select * from tblcontacts where userid = '".$client."' order by id desc";
	                          $clinet_query123 = $this->db->query($query1);	
	                          $client_emailfetch= $clinet_query123->row_array();
							if($client_emailfetch['email'] != "") { $client_email = $client_emailfetch['email']; } else { $client_email = "";}
                            echo render_input1('mail_to','Mail To',$client_email,'text',array(),array(),'','',2,'true');
                            ?>
                         <?php echo render_input1('cc','CC','','text',array(),array(),'','',2); ?>
                        <?php echo render_input1('email_subject','Subject','','text',array(),array(),'','',2); ?>
						
						
						</div>
                       
                        <hr />
                        <h5 class="bold"><?php echo _l('invoice_send_to_client_preview_template'); ?></h5>
                        <hr />
                        <?php echo form_hidden('client_id', $client); ?>
                        <?php echo form_hidden('project_id', $rel_id); ?>
                        <?php echo form_hidden('task_id', $task_id); ?>
                        <?php echo render_textarea('email_content','','',array(),array(),'','tinymce-'.$task_id); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
                <button type="submit" autocomplete="off" data-loading-text="<?php echo _l('wait_text'); ?>" class="btn btn-info"><?php echo _l('send'); ?></button>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
<script>
_validate_form($('#task-email-form'), {
      name: 'required',
      startdate: 'required'
    },task_email_form_handler);
</script>
