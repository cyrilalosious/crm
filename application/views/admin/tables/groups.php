<?php

defined('BASEPATH') or exit('No direct script access allowed');
$aColumns = [
    'name',
    ];

$sIndexColumn = 'id';
$sTable       = 'tblgroups';

$result  = data_tables_init($aColumns, $sIndexColumn, $sTable, [], [], ['id']);
$output  = $result['output'];
$rResult = $result['rResult'];

foreach ($rResult as $aRow) {
    $row = [];
    for ($i = 0; $i < count($aColumns); $i++) {
        $_data = $aRow[$aColumns[$i]];
        if ($aColumns[$i] == 'name') {
            $role_permissions = $this->ci->roles_model->get_role_permissions($aRow['id']);
            $_data            = '<a href="' . admin_url('groups/group/' . $aRow['id']) . '" class="mbot10 display-block">' . $_data . '</a>';
           
        }
        $row[] = $_data;
    }

    $options = icon_btn('groups/group/' . $aRow['id'], 'pencil-square-o');
	$options .= icon_btn('groups/subgroup/' . $aRow['id'], 'file');
    $row[]   = $options .= icon_btn('groups/delete/' . $aRow['id'], 'remove', 'btn-danger _delete');

    $output['aaData'][] = $row;
}
